package com.project.web.controller;

import com.project.web.pojo.request.AddFilmRequest;
import com.project.web.pojo.response.ListFilmResponse;
import com.project.web.sevice.AdminFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminFilmController {

    private AdminFilmService adminFilmService;

    @Autowired
    public AdminFilmController(AdminFilmService adminFilmService) {
        this.adminFilmService = adminFilmService;
    }

    @GetMapping(value = "/film-list", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<ListFilmResponse> listFilms() {
        return adminFilmService.findAllFilms();
    }

    @PostMapping("/add-film")
    @ResponseStatus(HttpStatus.OK)
    public void addFilm(@RequestBody @Valid AddFilmRequest addFilmRequest) {
        adminFilmService.addFilm(addFilmRequest);
    }

    @PostMapping("/removeFilm/{filmId}")
    @ResponseStatus(HttpStatus.OK)
    public void removeFilmFromStock(@PathVariable("filmId") long filmId) {
        adminFilmService.removeFilmFromStock(filmId);
    }

    @PostMapping("/changeFilmType")
    @ResponseStatus(HttpStatus.OK)
    public void changeFilmType(@RequestParam("filmId") long filmId, @RequestParam("filmType") String filmType) {
        adminFilmService.changeFilmType(filmId, filmType);
    }
}
