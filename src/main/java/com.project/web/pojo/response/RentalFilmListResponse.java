package com.project.web.pojo.response;

public class RentalFilmListResponse {

    private long id;
    private String name;
    private String filmType;
    private Long price;
    private String priceType;
    private String currencyCode;
    private Integer allowedIntervalInDays;

    public RentalFilmListResponse() {
    }

    public RentalFilmListResponse(long id, String name, String filmType, Long price, String priceType, String currencyCode, Integer allowedIntervalInDays) {
        this.id = id;
        this.name = name;
        this.filmType = filmType;
        this.price = price;
        this.priceType = priceType;
        this.currencyCode = currencyCode;
        this.allowedIntervalInDays = allowedIntervalInDays;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilmType() {
        return filmType;
    }

    public void setFilmType(String filmType) {
        this.filmType = filmType;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getAllowedIntervalInDays() {
        return allowedIntervalInDays;
    }

    public void setAllowedIntervalInDays(Integer allowedIntervalInDays) {
        this.allowedIntervalInDays = allowedIntervalInDays;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }
}
