package com.project.web.controller;

import com.project.web.pojo.request.CalculatePriceRequest;
import com.project.web.pojo.request.RentRequest;
import com.project.web.pojo.response.CalculatedPriceResponse;
import com.project.web.pojo.response.RentFilmResponse;
import com.project.web.pojo.response.RentalFilmListResponse;
import com.project.web.sevice.UserFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserFimController {

    private UserFilmService filmService;

    @Autowired
    public UserFimController(UserFilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping(value = "/rental-film-list", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<RentalFilmListResponse> getRentalFilmsWithPrice() {
        return filmService.listRentalFilmsWithPrice();
    }

    @PostMapping(value = "/calculate-price", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody CalculatedPriceResponse calculateFilmPrice(@RequestBody @Valid CalculatePriceRequest calculatePriceRequest) {
        return filmService.calculateFilmPricePerInterval(calculatePriceRequest);
    }

    @PostMapping(value = "/rent-film", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody RentFilmResponse rentFilm(@RequestBody @Valid RentRequest rentRequest) {
        return filmService.rentFilm(rentRequest);
    }


}
