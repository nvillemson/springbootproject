1) As an extra task implement a simple backend app (for example as REST services)
   which internally is using your created data model and API

   I used spring-boot-starter-test integration tests for that.

2) Requests are distributed through AdminFilmController and UserFimController
/film-list
/add-film
/removeFilm/{filmId}
/changeFilmType
/rental-film-list
/calculate-price
/rent-film
These requests should cover most functionality stated in the ticket.

3) Error handling is performed by RestExceptionHandler.

4) Project uses in-memory database and spring boot.

5) I compiled the project using Java 11. I am not sure if it will compile on lower versions.
Was using Java 8 in seeral places, inculded tests, data structures as LocalDate.

6) Rest of it, only Java 11 is mostly needed for starting the project.
Integration tests (spring boot based) are basically also replacement for external API.