package com.project.web.exceptions;

public class InsuffiientFundsException extends RuntimeException {

    public InsuffiientFundsException(String message) {
        super(message);
    }
}
