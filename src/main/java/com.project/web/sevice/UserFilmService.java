package com.project.web.sevice;

import com.project.persistence.additional.FilmType;
import com.project.persistence.additional.Price;
import com.project.persistence.model.Film;
import com.project.persistence.model.User;
import com.project.persistence.repository.FilmRepository;
import com.project.persistence.repository.UserRepository;
import com.project.web.exceptions.InsuffiientFundsException;
import com.project.web.pojo.request.CalculatePriceRequest;
import com.project.web.pojo.request.RentRequest;
import com.project.web.pojo.response.CalculatedPriceResponse;
import com.project.web.pojo.response.RentFilmResponse;
import com.project.web.pojo.response.RentalFilmListResponse;
import com.project.web.sevice.general.AbstractFilmService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class UserFilmService extends AbstractFilmService {

    private static final int BONUS_POINTS_AMOUNT_PER_DAY = 25;

    public UserFilmService(FilmRepository filmRepository, UserRepository userRepository) {
        super(filmRepository, userRepository);
    }

    public List<RentalFilmListResponse> listRentalFilmsWithPrice() {
        List<Film> films = filmRepository.listRentalFilms();

        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(Film.class, RentalFilmListResponse.class)
                .field("id", "id")
                .field("name", "name")
                .field("filmType", "filmType")
                .byDefault()
                .register();
        MapperFacade mapperFacade = mapperFactory.getMapperFacade();
        List<RentalFilmListResponse> rentalFilmListResponses = mapperFacade.mapAsList(films, RentalFilmListResponse.class);

        rentalFilmListResponses.forEach(film -> {
            FilmType filmType = FilmType.valueOf(film.getFilmType());
            int allowedIntervalInDays = filmType.getAllowedIntervalInDays();
            Price price = filmType.getWithinIntervalPrice();
            film.setAllowedIntervalInDays(allowedIntervalInDays);
            film.setCurrencyCode(price.getCurrencyCode());
            film.setPrice((long) price.getPrice());
            film.setPriceType(normaliseName(price.name()));
            film.setFilmType(normaliseName(film.getFilmType()));
        });

        return rentalFilmListResponses;
    }

    public CalculatedPriceResponse calculateFilmPricePerInterval(CalculatePriceRequest calculatePriceRequest) {
        Long filmId = calculatePriceRequest.getFilmId();
        Film actualFilm = getFilmById(filmId);

        LocalDate from = calculatePriceRequest.getFrom();
        LocalDate to = calculatePriceRequest.getTo();
        long rentDuration = validateRentDuration(from, to);

        FilmType filmType = actualFilm.getFilmType();
        Price withinIntervalPrice = filmType.getWithinIntervalPrice();
        long finalPrice = getFinalPrice(rentDuration, filmType);

        return new CalculatedPriceResponse(filmId, finalPrice, actualFilm.getName(), rentDuration,
                normaliseName(actualFilm.getFilmType().name()), withinIntervalPrice.getCurrencyCode());
    }

    public RentFilmResponse rentFilm(RentRequest rentRequest) {
        Long userId = rentRequest.getUserId();
        User user = getUserById(userId);

        Long filmId = rentRequest.getFilmId();
        Film actualFilm = getFilmById(filmId);

        FilmType filmType = actualFilm.getFilmType();
        LocalDate from = rentRequest.getFrom();
        LocalDate to = rentRequest.getTo();
        long rentDuration = validateRentDuration(from, to);
        Integer userBonusPoints = user.getBonusPoints();

        // bonus points are taken out automatically per new release
        Integer bonusPointsRemainder = userBonusPoints;
        if (filmType.equals(FilmType.NEW_RELEASE) && user.getBonusPoints() >= BONUS_POINTS_AMOUNT_PER_DAY) {
            int daysFree = userBonusPoints / BONUS_POINTS_AMOUNT_PER_DAY;
            if (daysFree >= rentDuration) {
                daysFree = (int) (daysFree - rentDuration + 1);
                rentDuration = 1;
            } else {
                rentDuration -= daysFree;
            }
            bonusPointsRemainder = userBonusPoints - daysFree * BONUS_POINTS_AMOUNT_PER_DAY;
        }

        Long payment = rentRequest.getPayment();
        long finalPrice = validatePriceAndPayment(filmType, rentDuration, payment);
        int returnAmount = calculateReturnAmount(payment, finalPrice);
        bonusPointsRemainder = increaseBonusRemainder(filmType, bonusPointsRemainder);

        if (!bonusPointsRemainder.equals(userBonusPoints)) {
            userRepository.updateUserBonusPoints(userId, bonusPointsRemainder);
        }
        filmRepository.setFilmAsRented(filmId, from, to, user);

        return new RentFilmResponse(actualFilm.getName(), returnAmount, rentRequest.getCurrencyCode(), from, to);
    }

    private int calculateReturnAmount(Long payment, long finalPrice) {
        int returnAmount = 0;
        if (payment > finalPrice) {
            returnAmount = (int) (payment - finalPrice);
        }
        return returnAmount;
    }

    private long validatePriceAndPayment(FilmType filmType, long rentDuration, Long payment) {
        long finalPrice = getFinalPrice(rentDuration, filmType);
        if (finalPrice > payment) {
            throw new InsuffiientFundsException("Actual price is: " + finalPrice + " EUR but customer only has " + payment + " EUR.");
        }
        return finalPrice;
    }

    private Integer increaseBonusRemainder(FilmType filmType, Integer bonusPointsRemainder) {
        if (filmType.equals(FilmType.NEW_RELEASE)) {
            return bonusPointsRemainder += 2;
        }
        return bonusPointsRemainder += 1;
    }
}
