package general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.Application;
import com.project.persistence.model.User;
import com.project.persistence.repository.UserRepository;
import com.project.web.pojo.request.AddFilmRequest;
import com.project.web.pojo.request.CalculatePriceRequest;
import com.project.web.pojo.request.RentRequest;
import com.project.web.pojo.response.ListFilmResponse;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public abstract class AbstractTest {

    @LocalServerPort
    private int port;

    protected ObjectMapper mapper;

    // admin
    protected String addFilmUrl;
    protected String listFilmUrl;
    protected String removeFilmUrl;
    protected String updateFilmTypeUrl;

    // user
    protected String rentalFilmListUrl;
    protected String calculatePriceUrl;
    protected String rentFilmUrl;

    @Autowired
    protected TestRestTemplate template;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        URL base = new URL("http://localhost:" + port + "/");
        addFilmUrl = base.toString() + "admin/add-film";
        listFilmUrl = base.toString() + "admin/film-list";
        removeFilmUrl = base.toString() + "admin/removeFilm/";
        updateFilmTypeUrl = base.toString() + "/admin/changeFilmType";
        rentalFilmListUrl = base.toString() + "/user/rental-film-list";
        calculatePriceUrl = base.toString() + "/user/calculate-price";
        rentFilmUrl = base.toString() + "/user/rent-film";
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }

    protected HttpEntity<String> produceFilmEntity(String filmName, String filmType) throws JsonProcessingException {
        AddFilmRequest addFilmRequest = new AddFilmRequest(filmName, filmType);

        String json = mapper.writeValueAsString(addFilmRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(json, headers);
    }

    protected HttpEntity<String> producePriceRequestEntity(Long filmId, LocalDate from, LocalDate to) throws JsonProcessingException {
        CalculatePriceRequest calculatePriceRequest = new CalculatePriceRequest(filmId, from, to);

        String json = mapper.writeValueAsString(calculatePriceRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(json, headers);
    }

    protected ResponseEntity<String> calculatePrice(Long filmId, LocalDate from, LocalDate to) throws JsonProcessingException {
        HttpEntity<String> entity = producePriceRequestEntity(filmId, from, to);
        ResponseEntity<String> response = template.postForEntity(calculatePriceUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        return response;
    }

    protected HttpEntity<String> produceFilmRentEntity(Long filmId, LocalDate from, LocalDate to, long userId, long payment) throws JsonProcessingException {
        RentRequest calculatePriceRequest = new RentRequest(filmId, from, to, userId, payment, "EUR");

        String json = mapper.writeValueAsString(calculatePriceRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(json, headers);
    }

    protected ResponseEntity<String> rentFilm(Long filmId, LocalDate from, LocalDate to, long userId, long payment) throws JsonProcessingException {
        HttpEntity<String> entity = produceFilmRentEntity(filmId, from, to, userId, payment);
        ResponseEntity<String> response = template.postForEntity(rentFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        return response;
    }

    protected void insertFilm(String filmName, String filmType) throws JsonProcessingException {
        HttpEntity<String> entity = produceFilmEntity(filmName, filmType);
        ResponseEntity<String> response = template.postForEntity(addFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    protected User saveUser(String userName, String password, User.UerRole userRole, Integer bonusPoints) {
        User user = new User(userName, password, userRole, bonusPoints);
        User savedUser = userRepository.save(user);
        return savedUser;
    }

    protected ListFilmResponse searchFilmWithinFoundFilms(List<ListFilmResponse> foundFilms, String filmName) {
        return foundFilms.stream()
                .filter(film -> filmName.equals(film.getName()))
                .findAny()
                .orElse(null);
    }
}
