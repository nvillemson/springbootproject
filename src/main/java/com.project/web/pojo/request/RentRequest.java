package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class RentRequest extends CalculatePriceRequest {

    @NotNull
    private Long userId;

    @NotNull
    private Long payment;

    @NotNull
    private String currencyCode;

    public RentRequest() {
    }

    public RentRequest(@NotNull Long filmId, @NotNull LocalDate from, @NotNull LocalDate to, @NotNull Long userId, @NotNull Long payment, @NotNull String currencyCode) {
        super(filmId, from, to);
        this.userId = userId;
        this.payment = payment;
        this.currencyCode = currencyCode;
    }


    public Long getPayment() {
        return payment;
    }

    public void setPayment(Long payment) {
        this.payment = payment;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
