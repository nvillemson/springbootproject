package com.project.web.sevice.general;

import com.project.persistence.additional.FilmType;
import com.project.persistence.additional.Price;
import com.project.persistence.model.Film;
import com.project.persistence.model.User;
import com.project.persistence.repository.FilmRepository;
import com.project.persistence.repository.UserRepository;
import com.project.web.exceptions.FilmNotFoundException;
import com.project.web.exceptions.NegativeRentDurationException;
import com.project.web.exceptions.UnknownFilmTypeException;
import com.project.web.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

public abstract class AbstractFilmService {

    protected FilmRepository filmRepository;
    protected UserRepository userRepository;

    @Autowired
    public AbstractFilmService(FilmRepository filmRepository, UserRepository userRepository) {
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    protected String normaliseName(String name) {
        return StringUtils.capitalize(name.replace("_", " ").toLowerCase());
    }

    protected FilmType validateFilmType(String rawFilmType) {
        FilmType filmType;
        try {
            filmType = FilmType.valueOf(rawFilmType);
        } catch (Exception ex) {
            throw new UnknownFilmTypeException("There is no suh film type " + rawFilmType + ".");
        }
        return filmType;
    }

    protected long getFinalPrice(long rentDuration, FilmType filmType) {
        Price withinIntervalPrice = filmType.getWithinIntervalPrice();
        int price = withinIntervalPrice.getPrice();
        // standard price on time not for new release
        long finalPrice = (long) price;
        // new release prie or overdue price
        if (filmType.equals(FilmType.NEW_RELEASE)) {
            finalPrice = price * rentDuration;
        } else {
            int allowedIntervalInDays = filmType.getAllowedIntervalInDays();
            if (rentDuration > allowedIntervalInDays) {
                finalPrice = price * (rentDuration - allowedIntervalInDays) + price;
            }
        }
        return finalPrice;
    }

    protected long validateRentDuration(LocalDate from, LocalDate to) {
        long rentDuration = DAYS.between(from, to);
        if (rentDuration == 0) {
            rentDuration = 1;
        }
        if (rentDuration < 0) {
            throw new NegativeRentDurationException("Rent start has to be before Rent end.");
        }
        return rentDuration;
    }

    protected Film getFilmById(Long filmId) {
        Optional<Film> film = filmRepository.findById(filmId);
        if (!film.isPresent()) {
            throw new FilmNotFoundException("There is no film with id :" + filmId + ".");
        }
        return film.get();
    }

    protected User getUserById(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new UserNotFoundException("There is no user with id :" + userId + ".");
        }
        return user.get();
    }
}
