package com.project.web.exceptions;


public class UnknownFilmTypeException extends RuntimeException {

    public UnknownFilmTypeException(String message) {
        super(message);
    }
}
