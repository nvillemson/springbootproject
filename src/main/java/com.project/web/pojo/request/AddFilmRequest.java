package com.project.web.pojo.request;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

public class AddFilmRequest {

    @NotNull
    private String filmName;

    @NotNull
    private String filmType;

    public AddFilmRequest(String filmName, String filmType) {
        this.filmName = filmName;
        this.filmType = filmType;
    }

    public AddFilmRequest() {
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public String getFilmType() {
        return filmType;
    }

    public void setFilmType(String filmType) {
        this.filmType = filmType;
    }
}
