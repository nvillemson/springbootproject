package com.project.persistence.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USER")
public class User {

    public User(String userName, String password, UerRole userRole, Integer bonusPoints) {
        this.userName = userName;
        this.password = password;
        this.userRole = userRole;
        this.bonusPoints = bonusPoints;
    }

    public User() {
    }

    public enum UerRole {
        ROLE_ADMIN,
        ROLE_CUSTOMER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "USER_NAME", nullable = false)
    private String userName;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "USER_ROLE", nullable = false)
    @Enumerated(EnumType.STRING)
    private UerRole userRole;

    @Column(name = "BONUS_POINTS")
    private Integer bonusPoints;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    public List<Film> films;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UerRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UerRole userRole) {
        this.userRole = userRole;
    }

    public Integer getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(Integer bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }

}
