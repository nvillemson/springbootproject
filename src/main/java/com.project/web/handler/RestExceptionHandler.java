package com.project.web.handler;

import com.project.web.exceptions.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UnknownFilmTypeException.class)
    protected ResponseEntity<String> handleFilmTypeNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(FilmNotFoundException.class)
    protected ResponseEntity<String> handleFilmNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(NegativeRentDurationException.class)
    protected ResponseEntity<String> handleNegativeRentDuration(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    protected ResponseEntity<String> handleUserNotFound(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

    @ExceptionHandler(InsuffiientFundsException.class)
    protected ResponseEntity<String> handleInsufficientFunds(Exception ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

}
