package com.project.persistence.repository;

import com.project.persistence.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface UserRepository extends CrudRepository<User, Long> {

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE User u SET u.bonusPoints = :bonusPointsRemainder WHERE u.id = :userId")
    void updateUserBonusPoints(Long userId, Integer bonusPointsRemainder);

}
