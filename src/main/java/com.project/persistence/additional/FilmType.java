package com.project.persistence.additional;

public enum FilmType {
    NEW_RELEASE(Price.PREMIUM_PRICE, 1),
    REGULAR_FILM(Price.BASIC_PRICE, 3),
    OLD_FILM(Price.BASIC_PRICE, 5);

    private Price withinIntervalPrice;
    private int allowedIntervalInDays;

    FilmType(Price withinIntervalPrice, int allowedIntervalInDays) {
        this.withinIntervalPrice = withinIntervalPrice;
        this.allowedIntervalInDays = allowedIntervalInDays;
    }

    public Price getWithinIntervalPrice() {
        return withinIntervalPrice;
    }

    public int getAllowedIntervalInDays() {
        return allowedIntervalInDays;
    }
}