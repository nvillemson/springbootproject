package tests;

import com.project.persistence.model.User;
import com.project.web.pojo.response.CalculatedPriceResponse;
import com.project.web.pojo.response.ListFilmResponse;
import com.project.web.pojo.response.RentFilmResponse;
import com.project.web.pojo.response.RentalFilmListResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class UserControllerTest extends AbstractTest {

    @Test
    public void rentalFilmListTestSuccess() throws Exception {
        insertFilm("Star Wars", "NEW_RELEASE");
        insertFilm("Alien", "REGULAR_FILM");
        insertFilm("Star Trek", "OLD_FILM");

        ResponseEntity<String> response = template.getForEntity(rentalFilmListUrl,  String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<RentalFilmListResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));

        RentalFilmListResponse starWars = searchFilmWithinFoundFilmsNotRented(foundFilms, "Star Wars");
        assertEquals("Star Wars", starWars.getName());
        assertEquals("New release", starWars.getFilmType());
        assertEquals("Premium price", starWars.getPriceType());
        assertEquals(Long.valueOf(4), starWars.getPrice());
        assertEquals(Integer.valueOf(1), starWars.getAllowedIntervalInDays());

        RentalFilmListResponse alien = searchFilmWithinFoundFilmsNotRented(foundFilms, "Alien");
        assertEquals("Alien", alien.getName());
        assertEquals("Regular film", alien.getFilmType());
        assertEquals("Basic price", alien.getPriceType());
        assertEquals(Long.valueOf(3), alien.getPrice());
        assertEquals(Integer.valueOf(3), alien.getAllowedIntervalInDays());

        RentalFilmListResponse starTrek = searchFilmWithinFoundFilmsNotRented(foundFilms, "Star Trek");
        assertEquals("Star Trek", starTrek.getName());
        assertEquals("Old film", starTrek.getFilmType());
        assertEquals("Basic price", starTrek.getPriceType());
        assertEquals(Long.valueOf(3), starTrek.getPrice());
        assertEquals(Integer.valueOf(5), starTrek.getAllowedIntervalInDays());
    }

    @Test
    public void calculatePriceTestBadRequest() throws Exception {
        insertFilm("Inception", "REGULAR_FILM");

        ResponseEntity<String> response = template.getForEntity(rentalFilmListUrl,  String.class);
        List<RentalFilmListResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));
        RentalFilmListResponse inception = searchFilmWithinFoundFilmsNotRented(foundFilms, "Inception");
        assertNotNull(inception);
        assertEquals(inception.getName(), "Inception");

        // bad request, missing fields
        HttpEntity<String> entity = producePriceRequestEntity(inception.getId(), null, null);
        response = template.postForEntity(calculatePriceUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // film does not exist
        LocalDate today = LocalDate.now();
        LocalDate upTo = today.plusDays(2);
        entity = producePriceRequestEntity(10000L, today, upTo);
        response = template.postForEntity(calculatePriceUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("There is no film with id :10000.", response.getBody());

        // negative rent interval
        today = today.plusDays(7);
        entity = producePriceRequestEntity(inception.getId(), today, upTo);
        response = template.postForEntity(calculatePriceUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Rent start has to be before Rent end.", response.getBody());
    }

    @Test
    public void calculatePriceTestSuccess() throws Exception {
        // old film 2 days exceeded
        assertFilmPrice("Batman 1", "OLD_FILM", 7,9);
        // regular film 1 day exceeded
        assertFilmPrice("Batman 2", "REGULAR_FILM", 4,6);
        // new film for 2 days
        assertFilmPrice("Batman 3", "NEW_RELEASE", 2,8);
    }

    @Test
    public void rentMovieTestBadRequest() throws Exception {
        insertFilm("Game of Thrones", "REGULAR_FILM");

        ResponseEntity<String> response = template.getForEntity(rentalFilmListUrl,  String.class);
        List<RentalFilmListResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));
        RentalFilmListResponse movie = searchFilmWithinFoundFilmsNotRented(foundFilms, "Game of Thrones");
        assertNotNull(movie);
        assertEquals("Game of Thrones", movie.getName());

        LocalDate today = LocalDate.now();
        LocalDate upTo = today.plusDays(2);
        // userId does not exist
        HttpEntity<String> entity = produceFilmRentEntity(movie.getId(), today, upTo, 1000, 10);
        response = template.postForEntity(rentFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("There is no user with id :1000.", response.getBody());

        User user = saveUser("Tester", "test1234", User.UerRole.ROLE_CUSTOMER, 10);
        long userId = user.getId();

        // film does not exist
        entity = produceFilmRentEntity(100L, today, upTo, userId, 10);
        response = template.postForEntity(rentFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("There is no film with id :100.", response.getBody());

        // wrong interval
        entity = produceFilmRentEntity(movie.getId(), upTo, today, userId, 10);
        response = template.postForEntity(rentFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Rent start has to be before Rent end.", response.getBody());

        // insufficientFunds
        entity = produceFilmRentEntity(movie.getId(), today, upTo, userId, 1);
        response = template.postForEntity(rentFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Actual price is: 3 EUR but customer only has 1 EUR.", response.getBody());
    }

    @Test
    public void rentMovieTestSuccess() throws Exception {
        // successfulRent, +1 bonusPoint for regular
        insertFilm("Breaking Bad", "REGULAR_FILM");
        User user = saveUser("Tester", "test1234", User.UerRole.ROLE_CUSTOMER, 24);
        long userId = user.getId();

        ResponseEntity<String> response = template.getForEntity(rentalFilmListUrl,  String.class);
        List<RentalFilmListResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));
        RentalFilmListResponse breakingBad = searchFilmWithinFoundFilmsNotRented(foundFilms, "Breaking Bad");
        assertNotNull(breakingBad);

        LocalDate today = LocalDate.now();
        LocalDate upTo = today.plusDays(2);

        response = rentFilm(breakingBad.getId(), today, upTo, userId, 4);
        RentFilmResponse rentResponseActual = mapper.readValue(response.getBody(), RentFilmResponse.class);
        assertEquals(Integer.valueOf(1), rentResponseActual.getReturnedAmount());
        assertEquals("Breaking Bad", rentResponseActual.getFilmName());

        // fail, because payment too low
        insertFilm("Pulp Fiction", "NEW_RELEASE");

        response = template.getForEntity(rentalFilmListUrl,  String.class);
        foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));
        RentalFilmListResponse pulpFiction = searchFilmWithinFoundFilmsNotRented(foundFilms, "Pulp Fiction");
        assertNotNull(pulpFiction);

        // 8, but one day off due to 25 bonus points campaign
        HttpEntity<String> entity = produceFilmRentEntity(pulpFiction.getId(), today, upTo, userId, 1);
        response = template.postForEntity(rentFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Actual price is: 4 EUR but customer only has 1 EUR.", response.getBody());

        // 25 bonus points -> 1 day for free and 2 EUR returned
        response = rentFilm(pulpFiction.getId(), today, upTo, userId, 6);
        rentResponseActual = mapper.readValue(response.getBody(), RentFilmResponse.class);
        assertEquals(Integer.valueOf(2), rentResponseActual.getReturnedAmount());
        assertEquals("Pulp Fiction", rentResponseActual.getFilmName());

        // rented film not shown
        response = template.getForEntity(rentalFilmListUrl,  String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));
        breakingBad = searchFilmWithinFoundFilmsNotRented(foundFilms, "Breaking Bad");
        assertNull(breakingBad);
        pulpFiction = searchFilmWithinFoundFilmsNotRented(foundFilms, "Pulp Fiction");
        assertNull(pulpFiction);

        response = template.getForEntity(listFilmUrl,  String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<ListFilmResponse> output = Arrays.asList(mapper.readValue(response.getBody(), ListFilmResponse[].class));
        ListFilmResponse breakingBadInAdmin = searchFilmWithinFoundFilms(output, "Breaking Bad");
        assertEquals(today, breakingBadInAdmin.getRentedFrom());
        assertEquals(upTo, breakingBadInAdmin.getRentedTo());
        assertEquals(upTo, breakingBadInAdmin.getRentedTo());
        ListFilmResponse pulpFictionAdmin = searchFilmWithinFoundFilms(output, "Pulp Fiction");
        assertEquals(today, pulpFictionAdmin.getRentedFrom());
        assertEquals(upTo, pulpFictionAdmin.getRentedTo());
    }

    private void assertFilmPrice(String filmName, String filmType, int intervalInDays, int expectedPrice) throws IOException {
        insertFilm(filmName, filmType);

        ResponseEntity<String> response = template.getForEntity(rentalFilmListUrl,  String.class);
        List<RentalFilmListResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), RentalFilmListResponse[].class));
        RentalFilmListResponse movie = searchFilmWithinFoundFilmsNotRented(foundFilms, filmName);
        assertNotNull(movie);
        assertEquals(filmName, movie.getName());

        LocalDate today = LocalDate.now();
        LocalDate upTo = today.plusDays(intervalInDays);
        response = calculatePrice(movie.getId(), today, upTo);
        CalculatedPriceResponse calculatedPriceResponse = mapper.readValue(response.getBody(), CalculatedPriceResponse.class);
        assertEquals(expectedPrice, calculatedPriceResponse.getPrice().intValue());
        assertEquals(intervalInDays, calculatedPriceResponse.getIntervalInDays().intValue());
        assertEquals(filmName, calculatedPriceResponse.getFilmName());
    }

    private RentalFilmListResponse searchFilmWithinFoundFilmsNotRented(List<RentalFilmListResponse> foundFilms, String filmName) {
        return foundFilms.stream()
                .filter(film -> filmName.equals(film.getName()))
                .findAny()
                .orElse(null);
    }
}
