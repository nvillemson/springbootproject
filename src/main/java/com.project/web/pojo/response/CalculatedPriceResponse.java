package com.project.web.pojo.response;

public class CalculatedPriceResponse {

    private Long filmId;
    private Long price;
    private String filmName;
    private Long intervalInDays;
    private String filmType;
    private String currencyCode;

    public CalculatedPriceResponse() {
    }

    public CalculatedPriceResponse(Long filmId, Long price, String filmName, Long intervalInDays, String filmType, String currencyCode) {
        this.filmId = filmId;
        this.price = price;
        this.filmName = filmName;
        this.intervalInDays = intervalInDays;
        this.filmType = filmType;
        this.currencyCode = currencyCode;
    }


    public Long getFilmId() {
        return filmId;
    }

    public void setFilmId(Long filmId) {
        this.filmId = filmId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public Long getIntervalInDays() {
        return intervalInDays;
    }

    public void setIntervalInDays(Long intervalInDays) {
        this.intervalInDays = intervalInDays;
    }

    public String getFilmType() {
        return filmType;
    }

    public void setFilmType(String filmType) {
        this.filmType = filmType;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
