package com.project.persistence.repository;

import com.project.persistence.additional.FilmType;
import com.project.persistence.model.Film;
import com.project.persistence.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

public interface FilmRepository extends CrudRepository<Film, Long> {

    @Query("SELECT f FROM Film f WHERE f.removed IS NULL")
    List<Film> listAllNonRemovedFims();

    // RENTED FILM ?
    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Film f SET f.removed = CURRENT_TIMESTAMP() WHERE f.id = :filmId AND f.removed IS NULL")
    void removeFilmFromStock(long filmId);

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Film f SET f.filmType = :filmType WHERE f.id = :filmId AND f.removed IS NULL")
    void changeFilmType(long filmId, FilmType filmType);

    @Query("SELECT f FROM Film f WHERE f.removed IS NULL AND f.rentedFrom IS NULL AND f.rentedTo IS NULL")
    List<Film> listRentalFilms();

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Film f SET f.rentedFrom = :from, f.rentedTo = :to, f.user = :user WHERE f.id = :filmId AND f.removed IS NULL")
    void setFilmAsRented(Long filmId, LocalDate from, LocalDate to, User user);

}
