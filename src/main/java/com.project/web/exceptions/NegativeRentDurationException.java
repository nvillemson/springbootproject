package com.project.web.exceptions;

public class NegativeRentDurationException extends RuntimeException {

    public NegativeRentDurationException(String message) {
        super(message);
    }
}
