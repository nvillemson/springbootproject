package com.project.web.sevice;

import com.project.persistence.additional.FilmType;
import com.project.persistence.model.Film;
import com.project.persistence.repository.FilmRepository;
import com.project.persistence.repository.UserRepository;
import com.project.web.pojo.request.AddFilmRequest;
import com.project.web.pojo.response.ListFilmResponse;
import com.project.web.sevice.general.AbstractFilmService;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminFilmService extends AbstractFilmService {

    public AdminFilmService(FilmRepository filmRepository, UserRepository userRepository) {
        super(filmRepository, userRepository);
    }

    public List<ListFilmResponse> findAllFilms() {
        List<Film> films = filmRepository.listAllNonRemovedFims();

        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(Film.class, ListFilmResponse.class)
                .field("id", "id")
                .field("name", "name")
                .field("filmType", "filmType")
                .field("rentedFrom", "rentedFrom")
                .field("rentedTo", "rentedTo")
                .byDefault()
                .register();
        MapperFacade mapperFacade = mapperFactory.getMapperFacade();

        List<ListFilmResponse> listFilmResponses = mapperFacade.mapAsList(films, ListFilmResponse.class);
        listFilmResponses.forEach(film -> film.setFilmType(normaliseName(film.getFilmType())));

        return listFilmResponses;
    }

    public void addFilm(AddFilmRequest addFilmRequest) {
        FilmType filmType = validateFilmType(addFilmRequest.getFilmType());
        Film film = new Film(addFilmRequest.getFilmName(), filmType);
        filmRepository.save(film);
    }

    public void removeFilmFromStock(long filmId) {
        filmRepository.removeFilmFromStock(filmId);
    }

    public void changeFilmType(long filmId, String rawFilmType) {
        FilmType filmType = validateFilmType(rawFilmType);
        filmRepository.changeFilmType(filmId, filmType);
    }

}
