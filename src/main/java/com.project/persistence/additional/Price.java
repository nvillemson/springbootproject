package com.project.persistence.additional;

public enum Price {
    PREMIUM_PRICE(4,"EUR"),
    BASIC_PRICE(3, "EUR");

    private int price;
    private String currencyCode;

    Price(int price, String currencyCode) {
        this.price = price;
        this.currencyCode = currencyCode;
    }

    public int getPrice() {
        return price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}