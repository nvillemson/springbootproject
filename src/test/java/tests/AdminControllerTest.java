package tests;

import com.project.web.pojo.response.ListFilmResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.*;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class AdminControllerTest extends AbstractTest {

    @Test
    public void testAddFilmBadRequest() throws Exception {
        HttpEntity<String> entity = produceFilmEntity("Matrix", "wrongType");
        ResponseEntity<String> response = template.postForEntity(addFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("There is no suh film type wrongType.", response.getBody());

        entity = produceFilmEntity(null, "NEW_RELEASE");
        response = template.postForEntity(addFilmUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void addFimTestSuccess() throws Exception {
        insertFilm("Matrix 3", "NEW_RELEASE");
        insertFilm("Matrix 2", "REGULAR_FILM");
        insertFilm("Matrix 1", "OLD_FILM");

        ResponseEntity<String> response = template.getForEntity(listFilmUrl,  String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<ListFilmResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), ListFilmResponse[].class));

        ListFilmResponse matrix3 = searchFilmWithinFoundFilms(foundFilms, "Matrix 3");
        assertNotNull(matrix3);
        assertEquals("Matrix 3", matrix3.getName());
        assertEquals("New release", matrix3.getFilmType());

        ListFilmResponse matrix2 = searchFilmWithinFoundFilms(foundFilms, "Matrix 2");
        assertNotNull(matrix2);
        assertEquals("Matrix 2", matrix2.getName());
        assertEquals("Regular film", matrix2.getFilmType());

        ListFilmResponse matrix1 = searchFilmWithinFoundFilms(foundFilms, "Matrix 1");
        assertNotNull(matrix1);
        assertEquals("Matrix 1", matrix1.getName());
        assertEquals("Old film", matrix1.getFilmType());
    }

    @Test
    public void testFilmRemovalFromStock() throws Exception {
        insertFilm("Blade Runner 2049", "NEW_RELEASE");

        ResponseEntity<String> response = template.getForEntity(listFilmUrl,  String.class);
        List<ListFilmResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), ListFilmResponse[].class));

        ListFilmResponse bladeRunner = searchFilmWithinFoundFilms(foundFilms, "Blade Runner 2049");
        assertNotNull(bladeRunner);
        assertEquals("Blade Runner 2049", bladeRunner.getName());

        // remove film here
        response = template.postForEntity(removeFilmUrl + bladeRunner.getId(), null, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        response = template.getForEntity(listFilmUrl,  String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        foundFilms = Arrays.asList(mapper.readValue(response.getBody(), ListFilmResponse[].class));

        foundFilms.forEach(f -> assertNotEquals("Blade Runner 2049", f.getName()));
    }

    @Test
    public void testUpdateFilmType() throws Exception {
        insertFilm("Arrival", "NEW_RELEASE");

        ResponseEntity<String> response = template.getForEntity(listFilmUrl,  String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        List<ListFilmResponse> foundFilms = Arrays.asList(mapper.readValue(response.getBody(), ListFilmResponse[].class));

        ListFilmResponse arrival = searchFilmWithinFoundFilms(foundFilms, "Arrival");
        assertNotNull(arrival);
        assertEquals("New release", arrival.getFilmType());
        assertEquals("Arrival", arrival.getName());

        // switch film type
        String updateTypeUrl = updateFilmTypeUrl + "?filmId=" + arrival.getId() + "&filmType=REGULAR_FILM";
        response = template.postForEntity(updateTypeUrl, null, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(listFilmUrl,  String.class);
        foundFilms = Arrays.asList(mapper.readValue(response.getBody(), ListFilmResponse[].class));
        arrival = searchFilmWithinFoundFilms(foundFilms, "Arrival");
        assertNotNull(arrival);
        assertEquals("Regular film", arrival.getFilmType());
        assertEquals("Arrival", arrival.getName());
    }
}
